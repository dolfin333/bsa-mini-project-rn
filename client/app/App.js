import React, {useEffect} from 'react';
import {Provider} from 'react-redux';
import store from './store/store.js';
import {MainNavigator} from './routes';
import {Storage} from './services/services';
import {StorageKey} from './common/enums/enums';
import NavigationService from './navigation';
import {RouteNames} from './common/enums/route/route-names.enum';

const App = () => {
  useEffect(() => {
    Storage.get(StorageKey.TOKEN).then(token => {
      if (token) {
        NavigationService.navigate(RouteNames.APP);
      } else {
        NavigationService.navigate(RouteNames.AUTH);
      }
    });
  });
  return (
    <Provider store={store}>
      <MainNavigator
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </Provider>
  );
};

export default App;
