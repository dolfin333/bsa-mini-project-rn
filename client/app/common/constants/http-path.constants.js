import {SERVER_PORT, IP_ADDRESS, PROTOCOL} from 'react-native-dotenv';

const START_OF_HTTP_REQUEST = (
  PROTOCOL +
  IP_ADDRESS +
  ':' +
  SERVER_PORT
).toString();

export {START_OF_HTTP_REQUEST};
