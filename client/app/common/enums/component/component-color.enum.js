const ComponentColor = {
  BUTTON: '#1678c2',
  CLOSE_BUTTON: '#aaa',
  INPUT_TITLE: '#e7696e',
  ICON: '#333',
  EYE_ICON: 'grey',
  LOADER: 'black',
  ERROR_TEXT: '#e7696e',
  LOGO_TEXT: '#767676',
  AUTH_TITLE: '#00b5ad',
  META: '#888',
};
export {ComponentColor};
