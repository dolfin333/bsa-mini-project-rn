const ErrorsMessages = {
  emailIsAlreadyTaken: 'Email is already taken',
  usernameIsAlreadyTaken: 'Username is already taken',
  incorrectEmail: 'Incorrect email',
  incorrectPassword: 'Passwords do not match',
  incorrectData: 'Incorrect data',
  writeCommentText: 'Write some comment text',
  writePostText: 'Write some post text',
};

export {ErrorsMessages};
