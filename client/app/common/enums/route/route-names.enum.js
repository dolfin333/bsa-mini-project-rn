const RouteNames = {
  THREAD: 'Thread',
  PROFILE: 'Profile',
  POST: 'Post',
  LOGIN: 'Login',
  REGISTRATION: 'Registration',
  APP: 'App',
  AUTH: 'Auth',
};
export {RouteNames};
