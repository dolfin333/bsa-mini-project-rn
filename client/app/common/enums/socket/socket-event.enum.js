const SocketEvent = {
  LIKE: 'like',
  CREATE_ROOM: 'createRoom',
  LEAVE_ROOM: 'leaveRoom',
};

export {SocketEvent};
