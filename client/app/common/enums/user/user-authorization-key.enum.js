const UserAuthorizationKey = {
  PASSWORD: 'password',
  EMAIL: 'email',
  USERNAME: 'username',
};

export {UserAuthorizationKey};
