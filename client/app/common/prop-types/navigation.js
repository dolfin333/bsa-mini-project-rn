import PropTypes from 'prop-types';

const navigationType = PropTypes.shape({
  navigate: PropTypes.func.isRequired,
});

export {navigationType};
