import PropTypes from 'prop-types';
import {imageType} from './image';
import {commentType} from './comment';

const postType = PropTypes.exact({
  id: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  image: imageType,
  imageId: PropTypes.string,
  likeCount: PropTypes.string.isRequired,
  dislikeCount: PropTypes.string.isRequired,
  likeUsers: PropTypes.array.isRequired,
  dislikeUsers: PropTypes.array.isRequired,
  commentCount: PropTypes.string.isRequired,
  comments: PropTypes.arrayOf(commentType),
  postReactions: PropTypes.array,
  deletedAt: PropTypes.string,
  userId: PropTypes.string.isRequired,
  user: PropTypes.exact({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    status: PropTypes.string,
    image: imageType,
  }).isRequired,
});

export {postType};
