import React, {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {
  profileActionCreator,
  threadActionCreator,
} from '../../../store/actions';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ComponentColor} from '../../../common/enums/enums';
import {navigationType, userType} from '../../../common/prop-types/prop-types';
import {disconnect} from '../../../sockets/sockets';

const HeaderView = ({navigation, isMainPage, isExpandedPost, user}) => {
  const dispatch = useDispatch();

  const handleUserLogout = useCallback(
    () => dispatch(profileActionCreator.logout()),
    [dispatch],
  );

  const removeExpandedPost = useCallback(
    () => dispatch(threadActionCreator.toggleExpandedPost()),
    [dispatch],
  );

  const handleGoBack = () => {
    if (isExpandedPost) {
      removeExpandedPost();
    }
    navigation.goBack();
  };

  const onLogOut = async () => {
    disconnect(user);
    handleUserLogout();
    navigation.navigate('Auth');
  };

  const onProfile = () => {
    navigation.navigate('Profile');
  };
  return (
    <View style={styles.header}>
      {isMainPage ? (
        <TouchableOpacity onPress={onProfile}>
          <View style={styles.userInfo}>
            <Icon
              name="user-circle"
              size={30}
              color={ComponentColor.ICON}
              style={styles.userIcon}
            />
            <Text>{user?.username}</Text>
          </View>
        </TouchableOpacity>
      ) : (
        <View style={styles.arrowIcon}>
          <Icon
            name="arrow-left"
            size={25}
            color={ComponentColor.ICON}
            onPress={handleGoBack}
          />
        </View>
      )}
      <TouchableOpacity onPress={onLogOut}>
        <Icon name="sign-out" size={30} color={ComponentColor.ICON} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 15,
    marginBottom: 10,
    backgroundColor: 'white',
    width: '100%',
  },
  userInfo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userIcon: {
    marginRight: 5,
  },
});

HeaderView.defaultProps = {
  isMainPage: false,
  isExpandedPost: false,
  user: null,
};

HeaderView.propTypes = {
  navigation: navigationType.isRequired,
  isMainPage: PropTypes.bool,
  isExpandedPost: PropTypes.bool,
  user: userType,
};
export default HeaderView;
