import React from 'react';

import {StyleSheet, View} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import {ComponentColor} from '../../../common/enums/enums';

const Loader = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator
        animating={true}
        color={ComponentColor.LOADER}
        size={50}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});
export default Loader;
