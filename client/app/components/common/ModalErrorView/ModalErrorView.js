import React, {useState, useEffect} from 'react';
import {Modal, StyleSheet, Text, Pressable, View} from 'react-native';
import PropTypes from 'prop-types';
import {ComponentColor} from '../../../common/enums/enums';

const ModalErrorView = ({errMess, setErrMess}) => {
  const [modalVisible, setModalVisible] = useState(false);
  useEffect(() => {
    if (errMess !== '') {
      setModalVisible(true);
    }
  }, [errMess]);
  const onModalClose = () => {
    setErrMess('');
    setModalVisible(false);
  };
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent
        visible={modalVisible}
        onRequestClose={onModalClose}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{errMess}</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={onModalClose}>
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 25,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: ComponentColor.CLOSE_BUTTON,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 24,
    color: ComponentColor.ERROR_TEXT,
    fontWeight: 'bold',
  },
});

ModalErrorView.propTypes = {
  errMess: PropTypes.string.isRequired,
  setErrMess: PropTypes.func.isRequired,
};
export default ModalErrorView;
