import {useEffect, useState} from 'react';
import {userType} from '../../../common/prop-types/user';
import {connect} from '../../../sockets/sockets';

const Notifications = ({user}) => {
  const [isRoomCreated, setIsRoomCreated] = useState(false);
  useEffect(() => {
    if (!user) {
      return undefined;
    }

    if (!isRoomCreated) {
      connect(user);
      setIsRoomCreated(true);
    }
    return () => {};
  }, [user, isRoomCreated]);

  return null;
};

Notifications.defaultProps = {
  user: null,
};

Notifications.propTypes = {
  user: userType,
};
export default Notifications;
