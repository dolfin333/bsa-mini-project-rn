import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {useSelector} from 'react-redux';
import {navigationType} from '../../common/prop-types/prop-types';
import HeaderView from '../common/HeaderView/HeaderView';

const ProfileView = ({navigation}) => {
  const {user} = useSelector(state => ({
    user: state.profile.user,
  }));
  return (
    <View>
      <HeaderView navigation={navigation} isMainPage={false} />
      <View style={styles.userInfo}>
        <Image
          source={require('./../../assets/images/avatar-default.png')}
          style={styles.img}
        />
        <Text
          style={[styles.username, styles.userInfoText]}
          ellipsizeMode="tail"
          numberOfLines={1}>
          {user.username}
        </Text>
        <Text
          style={styles.userInfoText}
          ellipsizeMode="tail"
          numberOfLines={1}>
          {user.email}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  userInfo: {
    alignItems: 'center',
    marginTop: 30,
  },
  userInfoText: {
    fontSize: 18,
    borderBottomWidth: 1,
    color: '#333',
    borderColor: '#aaa',
    width: '50%',
    textAlign: 'center',
  },
  img: {
    width: 150,
    height: 150,
    borderRadius: 100,
  },
  username: {
    marginTop: 20,
    marginBottom: 15,
  },
});

ProfileView.propTypes = {
  navigation: navigationType.isRequired,
};

export default ProfileView;
