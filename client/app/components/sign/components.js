import LoginView from './components/LoginView/LoginView';
import RegistrationView from './components/RegistrationView/RegistrationView';

export {LoginView, RegistrationView};
