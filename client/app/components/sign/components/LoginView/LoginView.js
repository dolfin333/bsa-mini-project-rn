import React, {useState, useCallback} from 'react';
import {useDispatch} from 'react-redux';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {profileActionCreator} from '../../../../store/actions';
import {
  ErrorsMessages,
  RouteNames,
  ComponentColor,
  UserAuthorizationKey,
} from '../../../../common/enums/enums';
import {navigationType} from '../../../../common/prop-types/prop-types';
import ModalErrorView from '../../../common/ModalErrorView/ModalErrorView';

const LoginView = ({navigation}) => {
  const [userData, setUserData] = useState({email: '', password: ''});
  const [errMess, setErrMess] = useState('');
  const [hidePass, setHidePass] = useState(true);

  const dispatch = useDispatch();

  const onChange = (name, value) => {
    setUserData({...userData, [name]: value});
  };

  const onEmailChange = value => {
    onChange(UserAuthorizationKey.EMAIL, value);
  };

  const onPasswordChange = value => {
    onChange(UserAuthorizationKey.PASSWORD, value);
  };

  const handleLogin = useCallback(
    loginPayload => dispatch(profileActionCreator.login(loginPayload)),
    [dispatch],
  );

  const onEyePressed = () => {
    setHidePass(!hidePass);
  };

  const onLogin = async () => {
    try {
      await handleLogin(userData);
      navigation.navigate(RouteNames.APP);
    } catch (e) {
      if (
        e.toString().includes(ErrorsMessages.incorrectEmail) ||
        e.toString().includes(ErrorsMessages.incorrectPassword)
      ) {
        setErrMess(ErrorsMessages.incorrectData);
      }
    }
  };

  const onSignUpPress = () => {
    setUserData({email: '', password: ''});
    navigation.navigate(RouteNames.REGISTRATION);
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image
            source={require('./../../../../assets/images/kitty.png')}
            style={styles.img}
          />
          <Text style={styles.logoText}>Thread</Text>
        </View>
        <Text style={styles.title}>Login to your account</Text>
        <TextInput
          placeholder="Email"
          style={styles.input}
          onChangeText={onEmailChange}
          value={userData.email}
        />
        <View style={styles.passwordContainer}>
          <TextInput
            placeholder="Password"
            style={[styles.input, styles.passwordInput]}
            onChangeText={onPasswordChange}
            secureTextEntry={hidePass ? true : false}
            value={userData.password}
          />
          <Icon
            name={hidePass ? 'eye' : 'eye-slash'}
            size={25}
            style={styles.passwordIcon}
            color={ComponentColor.EYE_ICON}
            onPress={onEyePressed}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={onLogin}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        <View style={styles.message}>
          <Text>New to us?</Text>
          <TouchableOpacity onPress={onSignUpPress}>
            <Text style={styles.loginText}> Sign Up</Text>
          </TouchableOpacity>
        </View>
        <ModalErrorView errMess={errMess} setErrMess={setErrMess} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: '90%',
    alignItems: 'center',
  },
  logo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logoText: {
    marginLeft: 15,
    color: ComponentColor.LOGO_TEXT,
    fontWeight: '700',
    fontSize: 30,
  },
  img: {width: 100, height: 90},
  input: {
    borderWidth: 2,
    borderColor: '#ccc',
    marginBottom: 20,
    padding: 10,
    width: '100%',
    fontSize: 16,
    alignItems: 'center',
  },
  passwordInput: {
    paddingRight: 40,
  },
  passwordIcon: {
    marginLeft: -35,
    paddingVertical: 15,
  },
  inputTitle: {
    color: ComponentColor.INPUT_TITLE,
    fontSize: 16,
  },
  passwordContainer: {
    flexDirection: 'row',
    borderColor: '#000',
    width: '100%',
  },
  message: {
    flexDirection: 'row',
  },
  loginText: {
    color: 'blue',
  },
  title: {
    fontSize: 20,
    marginVertical: 20,
    color: ComponentColor.AUTH_TITLE,
    fontWeight: '700',
  },
  button: {
    backgroundColor: ComponentColor.BUTTON,
    borderColor: 'white',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 0,
    borderRadius: 5,
    width: '30%',
    marginBottom: 15,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
  },
});

LoginView.propTypes = {
  navigation: navigationType.isRequired,
};

export default LoginView;
