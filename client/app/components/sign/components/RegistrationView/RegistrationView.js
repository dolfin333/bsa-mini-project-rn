import React, {useState, useCallback} from 'react';
import {useDispatch} from 'react-redux';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Image,
} from 'react-native';
import isEmail from 'validator/lib/isEmail';
import Icon from 'react-native-vector-icons/FontAwesome';
import ModalErrorView from '../../../common/ModalErrorView/ModalErrorView';
import {profileActionCreator} from '../../../../store/actions';
import {
  ComponentColor,
  ErrorsMessages,
  RouteNames,
  UserAuthorizationKey,
} from '../../../../common/enums/enums';
import {navigationType} from '../../../../common/prop-types/prop-types';

const RegistrationView = ({navigation}) => {
  const [userData, setUserData] = useState({
    email: '',
    password: '',
    username: '',
  });
  const [hidePass, setHidePass] = useState(true);
  const [errMess, setErrMess] = useState('');
  const dispatch = useDispatch();

  const onChange = (name, value) => {
    setUserData({...userData, [name]: value});
  };

  const onEmailChange = value => {
    onChange(UserAuthorizationKey.EMAIL, value);
  };

  const onPasswordChange = value => {
    onChange(UserAuthorizationKey.PASSWORD, value);
  };

  const onUsernameChange = value => {
    onChange(UserAuthorizationKey.USERNAME, value);
  };

  const onEyePressed = () => {
    setHidePass(!hidePass);
  };

  const handleRegister = useCallback(
    registerPayload => dispatch(profileActionCreator.register(registerPayload)),
    [dispatch],
  );
  const onRegistration = async () => {
    try {
      if (isEmail(userData.email)) {
        await handleRegister(userData);
        navigation.navigate(RouteNames.APP);
      } else {
        setErrMess(ErrorsMessages.incorrectEmail);
      }
    } catch (e) {
      if (e.toString().includes(ErrorsMessages.emailIsAlreadyTaken)) {
        setErrMess(ErrorsMessages.emailIsAlreadyTaken);
      }
      if (e.toString().includes(ErrorsMessages.usernameIsAlreadyTaken)) {
        setErrMess(ErrorsMessages.usernameIsAlreadyTaken);
      }
    }
  };

  const onLoginPress = () => {
    setUserData({
      email: '',
      password: '',
      username: '',
    });
    navigation.goBack();
  };
  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image
            source={require('./../../../../assets/images/kitty.png')}
            style={styles.img}
          />
          <Text style={styles.logoText}>Thread</Text>
        </View>
        <Text style={styles.title}>Register for free account</Text>
        <TextInput
          placeholder="Email"
          style={styles.input}
          onChangeText={onEmailChange}
          value={userData.email}
        />
        <TextInput
          placeholder="Name"
          style={styles.input}
          onChangeText={onUsernameChange}
          value={userData.username}
        />
        <View style={styles.passwordContainer}>
          <TextInput
            placeholder="Password"
            style={[styles.input, styles.passwordInput]}
            onChangeText={onPasswordChange}
            secureTextEntry={hidePass ? true : false}
            value={userData.password}
          />
          <Icon
            name={hidePass ? 'eye' : 'eye-slash'}
            size={25}
            style={styles.passwordIcon}
            color={ComponentColor.EYE_ICON}
            onPress={onEyePressed}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={onRegistration}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
        <View style={styles.message}>
          <Text>Already with us?</Text>
          <TouchableOpacity onPress={onLoginPress}>
            <Text style={styles.loginText}> Login</Text>
          </TouchableOpacity>
        </View>
        <ModalErrorView errMess={errMess} setErrMess={setErrMess} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: '90%',
    alignItems: 'center',
  },
  img: {width: 100, height: 90},
  logo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logoText: {
    marginLeft: 15,
    color: ComponentColor.LOGO_TEXT,
    fontWeight: '700',
    fontSize: 30,
  },
  input: {
    borderWidth: 2,
    borderColor: '#ccc',
    marginBottom: 20,
    padding: 10,
    width: '100%',
    fontSize: 16,
    alignItems: 'center',
  },
  passwordInput: {
    paddingRight: 40,
  },
  passwordIcon: {
    marginLeft: -35,
    paddingVertical: 15,
  },
  inputTitle: {
    color: ComponentColor.INPUT_TITLE,
    fontSize: 16,
  },
  passwordContainer: {
    flexDirection: 'row',
    borderColor: '#000',
    width: '100%',
  },
  message: {
    flexDirection: 'row',
  },
  loginText: {
    color: 'blue',
  },
  title: {
    fontSize: 20,
    marginVertical: 20,
    color: ComponentColor.AUTH_TITLE,
    fontWeight: '700',
  },
  button: {
    backgroundColor: ComponentColor.BUTTON,
    borderColor: 'white',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 0,
    borderRadius: 5,
    width: '30%',
    marginBottom: 15,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
  },
});

RegistrationView.propTypes = {
  navigation: navigationType.isRequired,
};

export default RegistrationView;
