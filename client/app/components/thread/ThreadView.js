import React, {useCallback, useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, View, Switch, Text} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import AddPostView from './components/AddPostView/AddPostView';
import getSortedPosts from './helpers/get-sorted-posts/get-sorted-posts.helper';
import PostView from './components/PostView/PostView';
import HeaderView from '../common/HeaderView/HeaderView';
import {threadActionCreator, profileActionCreator} from '../../store/actions';
import {RouteNames} from './../../common/enums/enums';
import {navigationType} from '../../common/prop-types/prop-types';
import Loader from '../common/Loader/Loader';
import Notifications from '../common/NotificationView/NotificationView';

const Thread = ({navigation}) => {
  const {posts, user, hasMorePosts} = useSelector(state => ({
    posts: state.thread.posts,
    hasMorePosts: state.thread.hasMorePosts,
    user: state.profile.user,
  }));
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [isMounted, setIsMounted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [postsFilter, setPostsFilter] = useState({
    userId: undefined,
    from: 0,
    count: 10,
    isOwnPosts: null,
    isLikedPosts: null,
  });
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isMounted) {
      dispatch(profileActionCreator.loadCurrentUser());
      dispatch(threadActionCreator.loadPosts(postsFilter)).then(() => {
        postsFilter.from = postsFilter.count;
      });
      setIsMounted(true);
    }
  }, [isMounted]);

  const handlePostsLoad = filtersPayload => {
    return dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    return dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const getMorePosts = () => {
    setIsLoading(true);
    handleMorePostsLoad(postsFilter).then(() => {
      const {from, count} = postsFilter;
      setPostsFilter({...postsFilter, from: from + count});
      setIsLoading(false);
    });
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : user.id;
    postsFilter.from = 0;
    postsFilter.isOwnPosts = showOwnPosts ? null : true;
    postsFilter.isLikedPosts = null;
    handlePostsLoad(postsFilter).then(() => {
      setPostsFilter({...postsFilter, from: postsFilter.count});
    });
  };

  const toggleExpandedPost = useCallback(
    id => {
      dispatch(threadActionCreator.toggleExpandedPost(id));
    },
    [dispatch],
  );

  const handleExpandedPostToggle = id => {
    toggleExpandedPost(id);
    navigation.navigate({
      routeName: RouteNames.POST,
      params: {
        user,
        onPostLike: handlePostLike,
        onExpandedPostToggle: toggleExpandedPost,
        onCommentAdd: handleCommentAdd,
      },
    });
  };

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch],
  );

  const handlePostLike = useCallback(
    id => {
      dispatch(threadActionCreator.likePost(id));
    },
    [dispatch],
  );

  const handleCommentAdd = useCallback(
    commentPayload => dispatch(threadActionCreator.addComment(commentPayload)),
    [dispatch],
  );

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
  };

  const onScroll = ({nativeEvent}) => {
    if (isCloseToBottom(nativeEvent) && hasMorePosts && !isLoading) {
      getMorePosts();
    }
  };

  return (
    <View style={styles.container}>
      <HeaderView navigation={navigation} isMainPage user={user} />
      <ScrollView onScroll={onScroll}>
        <View style={styles.wrapper}>
          <View style={styles.switch}>
            <Switch
              trackColor={{false: '#ccc', true: 'rgb(33, 133, 208)'}}
              thumbColor="white"
              onValueChange={toggleShowOwnPosts}
              value={showOwnPosts}
            />
            <Text>Show only my posts</Text>
          </View>
          <AddPostView handlePostAdd={handlePostAdd} />
        </View>
        {user &&
          getSortedPosts(posts ?? []).map(post => (
            <PostView
              post={post}
              user={user}
              key={post.id}
              onPostLike={handlePostLike}
              onExpandedPostToggle={handleExpandedPostToggle}
            />
          ))}
        {isLoading && <Loader />}
      </ScrollView>
      <Notifications user={user} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  switch: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

Thread.propTypes = {
  navigation: navigationType.isRequired,
};

export default Thread;
