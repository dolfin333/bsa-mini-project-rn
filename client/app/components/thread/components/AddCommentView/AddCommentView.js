import React, {useState} from 'react';
import {View, StyleSheet, Button} from 'react-native';
import PropTypes from 'prop-types';
import ModalAddCommentView from './ModalAddComment/ModalAddComment';

const AddCommentView = ({handleCommentAdd, postId}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const onModalClose = () => {
    setModalVisible(false);
  };

  const onModalShow = () => {
    setModalVisible(true);
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.buttonWrapper}>
        <Button title="Add comment" onPress={onModalShow} />
      </View>
      {modalVisible && (
        <ModalAddCommentView
          onModalClose={onModalClose}
          onCommentAdd={handleCommentAdd}
          postId={postId}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {marginHorizontal: 10},
  buttonWrapper: {justifyContent: 'center'},
});

AddCommentView.propTypes = {
  handleCommentAdd: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
};

export default AddCommentView;
