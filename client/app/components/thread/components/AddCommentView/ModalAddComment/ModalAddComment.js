import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  TextInput,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
} from 'react-native';
import {
  ComponentColor,
  ErrorsMessages,
} from '../../../../../common/enums/enums';
import ModalErrorView from '../../../../common/ModalErrorView/ModalErrorView';

const ModalAddCommentView = ({onModalClose, onCommentAdd, postId}) => {
  const [body, setBody] = useState('');
  const [errMess, setErrMess] = useState('');

  const onBodyChange = value => {
    setBody(value);
  };

  const handleOnAddComment = async () => {
    if (!body) {
      setErrMess(ErrorsMessages.writeCommentText);
    } else {
      await onCommentAdd({postId, body});
      setBody('');
      onModalClose();
    }
  };

  const onClose = () => {
    setBody('');
    onModalClose();
  };
  return (
    <View style={styles.centeredView}>
      <View>
        <Modal animationType="fade" transparent onRequestClose={onClose}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Create new comment</Text>
              <View style={styles.textAreaContainer}>
                <TextInput
                  style={styles.textArea}
                  underlineColorAndroid="transparent"
                  placeholder="Write your comment!"
                  placeholderTextColor="grey"
                  numberOfLines={3}
                  maxHeight={80}
                  multiline
                  value={body}
                  onChangeText={onBodyChange}
                />
              </View>
              <View style={styles.buttons}>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={onClose}>
                  <Text style={styles.textStyle}>Close</Text>
                </Pressable>
                <Pressable style={styles.button} onPress={handleOnAddComment}>
                  <Text style={styles.textStyle}>Comment</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </Modal>
        <ModalErrorView errMess={errMess} setErrMess={setErrMess} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '90%',
  },
  buttons: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 15,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: ComponentColor.BUTTON,
  },
  buttonClose: {
    backgroundColor: ComponentColor.CLOSE_BUTTON,
    marginRight: 10,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 20,
  },
  textArea: {
    justifyContent: 'flex-start',
  },
  textAreaContainer: {
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 5,
    width: '100%',
  },
});

ModalAddCommentView.propTypes = {
  onModalClose: PropTypes.func.isRequired,
  onCommentAdd: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
};

export default ModalAddCommentView;
