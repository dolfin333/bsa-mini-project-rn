import React, {useState} from 'react';
import {View, StyleSheet, Button} from 'react-native';
import PropTypes from 'prop-types';
import ModalAddPostView from './ModalAddPostView.js/ModalAddPostView';
const AddPostView = ({handlePostAdd}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const onModalClose = () => {
    setModalVisible(false);
  };

  const onModalShow = () => {
    setModalVisible(true);
  };

  return (
    <View style={styles.wrapper}>
      <View style={styles.buttonWrapper}>
        <Button title="Add post" onPress={onModalShow} />
      </View>
      {modalVisible && (
        <ModalAddPostView
          onModalClose={onModalClose}
          onPostAdd={handlePostAdd}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {marginHorizontal: 10},
  buttonWrapper: {alignSelf: 'flex-end'},
});

AddPostView.propTypes = {
  handlePostAdd: PropTypes.func.isRequired,
};

export default AddPostView;
