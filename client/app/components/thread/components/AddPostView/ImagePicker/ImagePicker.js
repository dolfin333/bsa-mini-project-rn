import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {ComponentColor} from '../../../../../common/enums/enums';
import ModalErrorView from '../../../../common/ModalErrorView/ModalErrorView';

const ImagePicker = ({uploadImage}) => {
  const [filePath, setFilePath] = useState({});
  const [errMess, setErrMess] = useState('');

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        return false;
      }
    }
    return true;
  };

  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs write permission',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        setErrMess('Write permission err', err);
      }
      return false;
    }
    return true;
  };

  const checkResponse = response => {
    if (response.didCancel) {
      setErrMess('User cancelled camera picker');
      return;
    } else if (response.errorCode === 'camera_unavailable') {
      setErrMess('Camera not available on device');
      return;
    } else if (response.errorCode === 'permission') {
      setErrMess('Permission not satisfied');
      return;
    } else if (response.errorCode === 'others') {
      setErrMess(response.errorMessage);
      return;
    }
    return true;
  };

  const captureImage = async type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
      saveToPhotos: true,
    };
    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();
    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(options, response => {
        if (checkResponse(response)) {
          const img = response.assets[0];
          setFilePath(img);
          uploadImage({
            uri: img.uri,
            type: img.type,
            name: img.fileName,
            data: img.data,
          });
        }
      });
    }
  };

  const chooseFile = type => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    launchImageLibrary(options, response => {
      if (checkResponse(response)) {
        const img = response.assets[0];
        setFilePath(img);
        uploadImage({
          uri: img.uri,
          type: img.type,
          name: img.fileName,
          data: img.data,
        });
      }
    });
  };

  const onLaunchCameraPressed = () => {
    captureImage('photo');
  };

  const onChooseImagePressed = () => {
    chooseFile('photo');
  };

  return (
    <View style={styles.container}>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.button}
          onPress={onLaunchCameraPressed}>
          <Text style={styles.textStyle}>Launch Camera</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.button}
          onPress={onChooseImagePressed}>
          <Text style={styles.textStyle}>Choose Image</Text>
        </TouchableOpacity>
      </View>
      {filePath.uri && (
        <Image source={{uri: filePath.uri}} style={styles.imageStyle} />
      )}
      <ModalErrorView errMess={errMess} setErrMess={setErrMess} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    width: '100%',
  },
  titleText: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 20,
  },
  textStyle: {
    padding: 10,
    paddingVertical: 5,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: ComponentColor.BUTTON,
    marginBottom: 10,
    borderRadius: 10,
    width: 100,
  },
  imageStyle: {
    width: 150,
    height: 150,
    margin: 5,
  },
});
ImagePicker.propTypes = {
  uploadImage: PropTypes.func.isRequired,
};

export default ImagePicker;
