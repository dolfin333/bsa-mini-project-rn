import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  TextInput,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
} from 'react-native';
import ImagePicker from '../ImagePicker/ImagePicker';
import {image as imageService} from '../../../../../services/services';
import {
  ComponentColor,
  ErrorsMessages,
} from '../../../../../common/enums/enums';
import ModalErrorView from '../../../../common/ModalErrorView/ModalErrorView';

const ModalAddPostView = ({onModalClose, onPostAdd}) => {
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [errMess, setErrMess] = useState('');

  const onBodyChange = value => {
    setBody(value);
  };

  const handleUploadImg = file => {
    imageService
      .uploadImage(file)
      .then(({id: imageId, link: imageLink}) => {
        setImage({imageId, imageLink});
      })
      .catch(() => {})
      .finally(() => {});
  };

  const handleOnAddPost = async () => {
    if (!body) {
      setErrMess(ErrorsMessages.writePostText);
    } else {
      await onPostAdd({imageId: image?.imageId, body});
      setBody('');
      setImage(undefined);
      onModalClose();
    }
  };

  const onClose = () => {
    setBody('');
    setImage(undefined);
    onModalClose();
  };
  return (
    <View style={styles.centeredView}>
      <View>
        <Modal animationType="fade" transparent onRequestClose={onClose}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Create new post</Text>
              <ImagePicker uploadImage={handleUploadImg} />
              <View style={styles.textAreaContainer}>
                <TextInput
                  style={styles.textArea}
                  underlineColorAndroid="transparent"
                  placeholder="What is the news?"
                  placeholderTextColor="grey"
                  numberOfLines={3}
                  maxHeight={80}
                  multiline
                  value={body}
                  onChangeText={onBodyChange}
                />
              </View>
              <View style={styles.buttons}>
                <Pressable
                  style={[styles.button, styles.buttonClose]}
                  onPress={onClose}>
                  <Text style={styles.textStyle}>Close</Text>
                </Pressable>
                <Pressable style={styles.button} onPress={handleOnAddPost}>
                  <Text style={styles.textStyle}>Post</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </Modal>
        <ModalErrorView errMess={errMess} setErrMess={setErrMess} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: '90%',
  },
  buttons: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 15,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: ComponentColor.BUTTON,
  },
  buttonClose: {
    backgroundColor: ComponentColor.CLOSE_BUTTON,
    marginRight: 10,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 20,
  },
  textArea: {
    justifyContent: 'flex-start',
  },
  textAreaContainer: {
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 5,
    width: '100%',
  },
});
ModalAddPostView.propTypes = {
  onModalClose: PropTypes.func.isRequired,
  onPostAdd: PropTypes.func.isRequired,
};
export default ModalAddPostView;
