import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ComponentColor} from '../../../../common/enums/enums';
import {commentType, userType} from '../../../../common/prop-types/prop-types';
import {getFromNowTime} from './../../../../helpers/helpers';

const CommentView = ({comment, user: currentUser}) => {
  const {
    body,
    user,
    likeCountComment,
    dislikeCountComment,
    createdAt,
    updatedAt,
  } = comment;

  const {id: userId} = currentUser;
  const date = getFromNowTime(updatedAt);
  return (
    <View style={styles.comment}>
      <View style={styles.imgWrapper}>
        <Image
          source={
            user.image?.link
              ? {uri: user.image.link}
              : require('./../../../../assets/images/avatar-default.png')
          }
          style={styles.image}
        />
      </View>
      <View style={styles.commentBodyWrapper}>
        <View style={styles.commentBody}>
          <View style={styles.commentBodyHeader}>
            <Text style={styles.meta}>
              {updatedAt === createdAt ? 'commented by' : 'updated by'}{' '}
              {user.username}
              {' - '}
              {date}
            </Text>
            {user.id === userId && (
              <Icon name="ellipsis-h" size={15} color={ComponentColor.ICON} />
            )}
          </View>
          <Text style={styles.body}>{body}</Text>
        </View>
        <View style={styles.actions}>
          <View style={styles.action}>
            <Icon
              name="thumbs-up"
              size={15}
              style={styles.icon}
              color={ComponentColor.ICON}
            />
            <Text>{likeCountComment}</Text>
          </View>
          <View style={styles.action}>
            <Icon
              name="thumbs-down"
              size={15}
              style={styles.icon}
              color={ComponentColor.ICON}
            />
            <Text>{dislikeCountComment}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  comment: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
    paddingHorizontal: 10,
  },
  commentBodyWrapper: {
    marginRight: 10,
  },
  icon: {
    marginRight: 5,
  },
  actions: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  action: {
    flexDirection: 'row',
    marginRight: 15,
    alignItems: 'center',
  },
  commentBody: {
    width: Dimensions.get('window').width - 80,
  },
  commentBodyHeader: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  meta: {
    color: ComponentColor.META,
  },
  body: {
    fontSize: 16,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },
  imgWrapper: {
    margin: 5,
    marginLeft: 10,
  },
});

CommentView.defaultProps = {
  currentUser: null,
};

CommentView.propTypes = {
  comment: commentType.isRequired,
  currentUser: userType,
};

export default CommentView;
