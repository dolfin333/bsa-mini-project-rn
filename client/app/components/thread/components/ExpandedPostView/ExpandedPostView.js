import React from 'react';
import {useSelector} from 'react-redux';
import {View, Text, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import PostView from '../PostView/PostView';
import CommentView from '../CommentView/CommentView';
import HeaderView from '../../../common/HeaderView/HeaderView';
import {getSortedComments} from './helpers/get-sorted-comments/get-sorted-comments.helper';
import AddCommentView from '../AddCommentView/AddCommentView';
import {navigationType} from '../../../../common/prop-types/prop-types';

const ExpandedPost = ({navigation}) => {
  const {expandedPost} = useSelector(state => ({
    expandedPost: state.thread.expandedPost,
  }));
  const user = navigation.getParam('user');
  return (
    <View style={styles.container}>
      <HeaderView navigation={navigation} isExpandedPost />
      <View style={styles.container}>
        <ScrollView>
          {expandedPost && (
            <>
              <PostView
                post={expandedPost}
                user={user}
                onPostLike={navigation.getParam('onPostLike')}
                onExpandedPostToggle={navigation.getParam(
                  'onExpandedPostToggle',
                )}
              />
              <View style={styles.comments}>
                <View style={styles.wrapper}>
                  <View>
                    <Text style={styles.commentsText}>Comments</Text>
                  </View>
                  <View>
                    <AddCommentView
                      postId={expandedPost.id}
                      handleCommentAdd={navigation.getParam('onCommentAdd')}
                    />
                  </View>
                </View>
                {getSortedComments(expandedPost.comments).map(comment => (
                  <CommentView comment={comment} user={user} key={comment.id} />
                ))}
              </View>
            </>
          )}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  comments: {
    backgroundColor: 'white',
    flex: 1,
  },
  commentsText: {
    fontSize: 20,
    marginVertical: 10,
    marginLeft: 15,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

ExpandedPost.propTypes = {
  navigation: navigationType.isRequired,
};

export default ExpandedPost;
