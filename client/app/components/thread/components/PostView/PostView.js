import React, {useState} from 'react';
import {View, Text, StyleSheet, Image, Share} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ComponentColor} from '../../../../common/enums/enums';
import {postType, userType} from '../../../../common/prop-types/prop-types';
import {getFromNowTime} from './../../../../helpers/helpers';
import ModalErrorView from '../../../common/ModalErrorView/ModalErrorView';

const PostView = ({
  post,
  user: currentUser,
  onPostLike,
  onExpandedPostToggle,
}) => {
  const [errMess, setErrMess] = useState('');

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt,
  } = post;
  const {id: userId} = currentUser;
  const date = getFromNowTime(updatedAt);

  const onSharePressed = async () => {
    try {
      const result = await Share.share({
        message: `Share post with id ${id}`,
      });
    } catch (error) {
      setErrMess(error.message);
    }
  };

  const handlePostLike = () => {
    onPostLike(id);
  };
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);

  return (
    <View>
      <View style={styles.post}>
        {image?.link && (
          <Image
            source={{
              uri: image.link,
            }}
            style={styles.postImage}
          />
        )}
        <View style={styles.postBody}>
          <View style={styles.postBodyHeader}>
            <Text style={styles.meta}>
              {updatedAt === createdAt ? 'posted by' : 'updated by'}{' '}
              {user.username}
              {' - '}
              {date}
            </Text>
            {user.id === userId && (
              <Icon name="ellipsis-h" size={15} color={ComponentColor.ICON} />
            )}
          </View>
          <Text style={styles.body}>{body}</Text>
        </View>
        <View style={styles.actions}>
          <View style={styles.action}>
            <Icon
              name="thumbs-up"
              size={15}
              style={styles.icon}
              color={ComponentColor.ICON}
              onPress={handlePostLike}
            />
            <Text>{likeCount}</Text>
          </View>
          <View style={styles.action}>
            <Icon
              name="thumbs-down"
              size={15}
              style={styles.icon}
              color={ComponentColor.ICON}
            />
            <Text>{dislikeCount}</Text>
          </View>
          <View style={styles.action}>
            <Icon
              name="comment"
              size={15}
              style={styles.icon}
              color={ComponentColor.ICON}
              onPress={handleExpandedPostToggle}
            />
            <Text>{commentCount}</Text>
          </View>
          <Icon
            name="share-alt"
            size={15}
            color={ComponentColor.ICON}
            onPress={onSharePressed}
          />
        </View>
      </View>
      <ModalErrorView errMess={errMess} setErrMess={setErrMess} />
    </View>
  );
};

const styles = StyleSheet.create({
  post: {
    borderWidth: 1,
    marginHorizontal: 10,
    marginVertical: 5,
    borderRadius: 5,
    borderColor: '#aaa',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 2,
    elevation: 2,
    backgroundColor: 'white',
  },
  icon: {
    marginRight: 5,
  },
  actions: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#aaa',
    padding: 10,
    alignItems: 'center',
  },
  action: {
    flexDirection: 'row',
    marginRight: 15,
    alignItems: 'center',
  },
  postBody: {
    padding: 10,
  },
  postBodyHeader: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  meta: {
    color: ComponentColor.META,
  },
  body: {
    fontSize: 16,
  },
  postImage: {
    aspectRatio: 1,
  },
});

PostView.defaultProps = {
  currentUser: null,
};

PostView.propTypes = {
  post: postType.isRequired,
  currentUser: userType,
  onPostLike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
};

export default PostView;
