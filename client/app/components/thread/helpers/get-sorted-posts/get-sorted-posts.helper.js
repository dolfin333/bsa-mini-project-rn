import {getDiff} from './../../../../helpers/helpers';

const getSortedPosts = posts =>
  posts.slice().sort((a, b) => getDiff(b.createdAt, a.createdAt));

export default getSortedPosts;
