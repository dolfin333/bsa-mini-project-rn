import PushNotification from 'react-native-push-notification';

const createChannel = userId => {
  PushNotification.createChannel(
    {
      channelId: userId, // (required)
      channelName: 'threadChannel', // (required)
    },
    // created => console.log(`createChannel returned '${created}' ${userId}`), // (optional) false means it already existed.
  );
};

const deleteChannel = userId => {
  PushNotification.deleteChannel(userId);
};

// Must be outside of any component LifeCycle (such as `componentDidMount`).
PushNotification.configure({
  onRegister: function (token) {
    // console.log('TOKEN:', token);
  },
  onNotification: function (notification) {
    console.log('NOTIFICATION:', notification);
  },
  onAction: function (notification) {
    console.log('ACTION:', notification.action);
    console.log('NOTIFICATION:', notification);
  },
  onRegistrationError: function (err) {
    console.error(err.message, err);
  },
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  popInitialNotification: true,
  requestPermissions: true,
});

const Push = ({title = '', message = '', userId}) => {
  PushNotification.localNotification({
    channelId: userId,
    title: title, // (optional)
    message: message, // (required)
  });
};

export {Push, createChannel, deleteChannel};
