import {createStackNavigator} from 'react-navigation-stack';
import ProfileView from '../../components/profile/ProfileView';
import ExpandedPost from '../../components/thread/components/ExpandedPostView/ExpandedPostView';
import Thread from '../../components/thread/ThreadView';

const AppNavigator = createStackNavigator(
  {
    Thread: {
      screen: Thread,
      navigationOptions: {
        headerShown: false,
      },
    },
    Profile: {
      screen: ProfileView,
      navigationOptions: {
        headerShown: false,
      },
    },
    Post: {
      screen: ExpandedPost,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'Thread',
  },
);

export default AppNavigator;
