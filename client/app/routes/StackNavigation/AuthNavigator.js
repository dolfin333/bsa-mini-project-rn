import {createStackNavigator} from 'react-navigation-stack';
import {LoginView, RegistrationView} from './../../components/sign/components';

const AuthNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginView,
      navigationOptions: {
        headerShown: false,
      },
    },
    Registration: {
      screen: RegistrationView,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'Login',
  },
);

export default AuthNavigator;
