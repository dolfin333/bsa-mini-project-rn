import {HttpMethod, ContentType} from './../../common/enums/enums';

class Auth {
  constructor({http, START_OF_HTTP_REQUEST}) {
    this._http = http;
    this.START_OF_HTTP_REQUEST = START_OF_HTTP_REQUEST;
  }

  login(payload) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/auth/login`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload),
    });
  }

  registration(payload) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/auth/register`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload),
    });
  }

  async getCurrentUser() {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/auth/user`, {
      method: HttpMethod.GET,
    });
  }
}

export {Auth};
