import {HttpMethod, ContentType} from './../../common/enums/enums';

class Comment {
  constructor({http, START_OF_HTTP_REQUEST}) {
    this._http = http;
    this.START_OF_HTTP_REQUEST = START_OF_HTTP_REQUEST;
  }

  getComment(id) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/comments/${id}`, {
      method: HttpMethod.GET,
    });
  }

  addComment(payload) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/comments`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }
}

export {Comment};
