import {getStringifiedQuery} from './../../helpers/helpers';
import {StorageKey, HttpHeader, HttpMethod} from './../../common/enums/enums';

class Http {
  constructor(storage) {
    this._storage = storage;
  }

  async load(url, options = {}) {
    const {
      method = HttpMethod.GET,
      payload = null,
      hasAuth = true,
      contentType,
      query,
    } = options;
    const headers = await this._getHeaders({
      hasAuth,
      contentType,
    });

    return fetch(this._getUrl(url, query), {
      method,
      headers,
      body: payload,
    })
      .then(this._checkStatus)
      .then(this._parseJSON)
      .catch(this._throwError);
  }

  async _getHeaders({hasAuth, contentType}) {
    const headers = new Headers();

    if (contentType) {
      headers.append(HttpHeader.CONTENT_TYPE, contentType);
    }

    if (hasAuth) {
      const token = await this._storage.get(StorageKey.TOKEN);
      headers.append(HttpHeader.AUTHORIZATION, `Bearer ${token}`);
    }
    return headers;
  }

  async _checkStatus(response) {
    if (!response.ok) {
      const parsedException = await response.json();

      throw new Error(parsedException?.message ?? response.statusText);
    }

    return response;
  }

  _getUrl(url, query) {
    return `${url}${query ? `?${getStringifiedQuery(query)}` : ''}`;
  }

  _parseJSON(response) {
    return response.json();
  }

  _throwError(err) {
    throw err;
  }
}

export {Http};
