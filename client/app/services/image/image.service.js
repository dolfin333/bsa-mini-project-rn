import {HttpMethod} from './../../common/enums/enums';

class Image {
  constructor({http, START_OF_HTTP_REQUEST}) {
    this._http = http;
    this.START_OF_HTTP_REQUEST = START_OF_HTTP_REQUEST;
  }

  uploadImage(image) {
    const formData = new FormData();
    formData.append('image', image);
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/images`, {
      method: HttpMethod.POST,
      payload: formData,
    });
  }
}

export {Image};
