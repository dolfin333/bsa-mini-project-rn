import {HttpMethod, ContentType} from './../../common/enums/enums';

class Post {
  constructor({http, START_OF_HTTP_REQUEST}) {
    this._http = http;
    this.START_OF_HTTP_REQUEST = START_OF_HTTP_REQUEST;
  }

  getAllPosts(filter) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/posts`, {
      method: HttpMethod.GET,
      query: filter,
    });
  }

  getPost(id) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/posts/${id}`, {
      method: HttpMethod.GET,
    });
  }

  addPost(payload) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/posts`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  likePost(postId) {
    return this._http.load(`${this.START_OF_HTTP_REQUEST}/api/posts/like`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify({postId, isLike: true}),
    });
  }
}

export {Post};
