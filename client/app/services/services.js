import {START_OF_HTTP_REQUEST} from './../common/constants/constants';
import {Auth} from './auth/auth.service';
import {Http} from './http/http.service';
import {Post} from './post/post.service';
import {Image} from './image/image.service';
import {Comment} from './comment/comment.service';
import Storage from './storage/storage.service';

const http = new Http(Storage);

const auth = new Auth({
  http,
  START_OF_HTTP_REQUEST,
});

const post = new Post({
  http,
  START_OF_HTTP_REQUEST,
});

const image = new Image({
  http,
  START_OF_HTTP_REQUEST,
});

const comment = new Comment({
  http,
  START_OF_HTTP_REQUEST,
});

export {http, Storage, auth, post, image, comment};
