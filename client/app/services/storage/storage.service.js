import AsyncStorage from '@react-native-async-storage/async-storage';
import {APP_NAME_SPACE} from '../../common/constants/app-name-space.constants';

const set = async (key, value) => {
  try {
    await AsyncStorage.setItem(`${APP_NAME_SPACE}@${key}`, value);
  } catch (e) {
    console.log(e);
  }
};

const get = async key => {
  try {
    const value = await AsyncStorage.getItem(`${APP_NAME_SPACE}@${key}`);
    return value;
  } catch (e) {
    console.log(e);
  }
};

const remove = async key => {
  try {
    await AsyncStorage.removeItem(`${APP_NAME_SPACE}@${key}`);
    return true;
  } catch (exception) {
    return false;
  }
};

const Storage = {set, get, remove};

export default Storage;
