window.navigator.userAgent = 'react-native';
import io from 'socket.io-client';
import {SOCKET_PORT, IP_ADDRESS, PROTOCOL} from 'react-native-dotenv';
import {Push, createChannel, deleteChannel} from './../notifications';
import {PushTitle, SocketEvent} from '../common/enums/enums';

const socket = io(`${PROTOCOL}${IP_ADDRESS}:${SOCKET_PORT}`, {
  transports: ['websocket'],
  jsonp: false,
});

const connect = user => {
  socket.connect();
  createChannel(user.id);
  socket.emit(SocketEvent.CREATE_ROOM, user.id);
  socket.on(SocketEvent.LIKE, mess => {
    Push({message: mess, userId: user.id, title: PushTitle.CHECK_YOUR_ACCOUNT});
  });
};

const disconnect = user => {
  socket.emit(SocketEvent.LEAVE_ROOM, user.id);
  deleteChannel(user.id);
};

export {connect, disconnect};
