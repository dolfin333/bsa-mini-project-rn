import {createAction} from '@reduxjs/toolkit';
import {StorageKey} from '../../common/enums/enums';
import {Storage, auth as authService} from '../../services/services';
import ActionType from './actionsTypes';

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user,
  },
}));

const login = request => async dispatch => {
  try {
    const {user, token} = await authService.login(request);
    await Storage.set(StorageKey.TOKEN, token);
    dispatch(setUser(user));
  } catch (err) {
    throw err;
  }
};

const register = request => async dispatch => {
  try {
    const {user, token} = await authService.registration(request);
    await Storage.set(StorageKey.TOKEN, token);
    dispatch(setUser(user));
  } catch (err) {
    throw err;
  }
};

const logout = () => async dispatch => {
  await Storage.remove(StorageKey.TOKEN);
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await authService.getCurrentUser();
  dispatch(setUser(user));
};

export {setUser, login, register, logout, loadCurrentUser};
