const ActionType = {
  SET_USER: 'profile/set-user',
};

export default ActionType;
