import {createAction} from '@reduxjs/toolkit';
import ActionType from './actionsTypes';
import {
  post as postService,
  comment as commentService,
} from '../../services/services';

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts,
  },
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post,
  },
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts,
  },
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post,
  },
}));

const setExpandedPostComments = createAction(
  ActionType.SET_EXPANDED_POST_COMMENTS,
  comments => ({
    payload: {
      comments,
    },
  }),
);

const addUpdatePost = createAction(ActionType.UPDATE_POST, post => ({
  payload: {
    post,
  },
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    thread: {posts},
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)),
  );
  dispatch(addMorePosts(filteredPosts));
};

const createPost = post => async dispatch => {
  const {id} = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
  if (post) {
    dispatch(setExpandedPostComments(post.comments));
  }
};

function changeLikeCount(dispatch, getRootState, diff, postId) {
  const {
    thread: {posts, expandedPost},
    profile: {user},
  } = getRootState();
  const mapLikes = post => {
    let likeUsers = [...post.likeUsers];
    if (diff === 1) {
      const likeUser = {
        id: user.id,
        username: user.username,
        image: {id: user.image?.id, link: user.image?.link},
        status: user.status,
      };
      likeUsers.push(likeUser);
    } else {
      likeUsers = likeUsers.filter(
        likeUser => likeUser.userId ?? likeUser.id !== user.id,
      );
    }
    return {
      ...post,
      likeCount: (Number(post.likeCount) + diff).toString(), // diff is taken from the current closure
      likeUsers,
    };
  };

  const updated = posts.map(post =>
    post.id !== postId ? post : mapLikes(post),
  );
  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
}

const likePost = postId => async (dispatch, getRootState) => {
  const res = await postService.likePost(postId);
  const diff = res.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  changeLikeCount(dispatch, getRootState, diff, postId);
};

const addComment = request => async (dispatch, getRootState) => {
  const {id} = await commentService.addComment(request);
  const comment = await commentService.getComment(id);
  const {
    thread: {expandedPostComments, expandedPost},
  } = getRootState();
  const updatedComments = Array.from(expandedPostComments);
  updatedComments.push(comment);
  dispatch(setExpandedPostComments(updatedComments));
  const post = await postService.getPost(expandedPost.id);
  dispatch(setExpandedPost(post));
  dispatch(addUpdatePost(post));
};

export {
  setPosts,
  setExpandedPost,
  addPost,
  addMorePosts,
  setExpandedPostComments,
  addUpdatePost,
  loadPosts,
  loadMorePosts,
  createPost,
  likePost,
  toggleExpandedPost,
  addComment,
};
