const ActionType = {
  SET_ALL_POSTS: 'thread/set-all-posts',
  ADD_POST: 'thread/add-post',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_EXPANDED_POST_COMMENTS: 'thread/set-expanded-post-comments',
  UPDATE_POST: 'thread/update-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
};

export default ActionType;
