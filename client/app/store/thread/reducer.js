import {createReducer} from '@reduxjs/toolkit';
import {
  setPosts,
  addPost,
  addMorePosts,
  setExpandedPost,
  addUpdatePost,
  setExpandedPostComments,
} from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  expandedPostComments: [],
  hasMorePosts: true,
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setPosts, (state, action) => {
    const {posts} = action.payload;
    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(addPost, (state, action) => {
    const {post} = action.payload;
    state.posts = [post, ...state.posts];
  });
  builder.addCase(addMorePosts, (state, action) => {
    const {posts} = action.payload;
    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(setExpandedPost, (state, action) => {
    const {post} = action.payload;
    state.expandedPost = post;
  });
  builder.addCase(addUpdatePost, (state, action) => {
    const {post} = action.payload;
    state.posts = state.posts.filter(statePost => statePost.id !== post.id);
    state.posts = [post, ...state.posts];
  });
  builder.addCase(setExpandedPostComments, (state, action) => {
    const {comments} = action.payload;
    state.expandedPostComments = comments;
  });
});

export {reducer};
