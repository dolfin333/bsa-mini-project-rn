import { PostsApiPath } from "../../common/enums/enums";

const initPost = (Router, services) => {
  const { post: postService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) =>
      postService
        .getPosts(req.query)
        .then((posts) => {
          res.send(posts);
        })
        .catch(next)
    )
    .get(PostsApiPath.$ID, (req, res, next) =>
      postService
        .getPostById(req.params.id)
        .then((post) => {
          res.send(post);
        })
        .catch(next)
    )
    .post(PostsApiPath.ROOT, (req, res, next) =>
      postService
        .createPost(req.user.id, req.body)
        .then((post) => {
          Object.keys(req.io.sockets.sockets).forEach((socketid) =>
            req.io.to(socketid).emit("new_post", post)
          );
          // notify all users that a new post was created
          return res.send(post);
        })
        .catch(next)
    )
    .put(PostsApiPath.$ID, (req, res, next) =>
      postService
        .updatePost(req.params.id, req.body)
        .then((post) => {
          req.io.emit("update_post", post); // notify all users that a post was updated
          return res.send(post);
        })
        .catch(next)
    )
    .delete(PostsApiPath.$ID, (req, res, next) =>
      postService
        .deletePost(req.params.id)
        .then((post) => {
          req.io.emit("delete_post", post); // notify all users that a post was deleted
          return res.send(post);
        })
        .catch(next)
    )
    .post(PostsApiPath.LIKE, (req, res, next) =>
      postService
        .setLike(req.user.id, req.body)
        .then((reaction) => {
          if (reaction.post && reaction.post.userId !== req.user.id) {
            req.io
              .to(reaction.post.userId)
              .emit("like", "Your post was liked!");
          }
          return res.send(reaction);
        })
        .catch(next)
    )
    .post(PostsApiPath.DISLIKE, (req, res, next) =>
      postService
        .setDislike(req.user.id, req.body)
        .then((reaction) => {
          if (reaction.post && reaction.post.userId !== req.user.id) {
            // notify a user if someone (not himself) disliked his pos
            req.io
              .to(reaction.post.userId)
              .emit("dislike", "Your post was disliked!");
          }
          return res.send(reaction);
        })
        .catch(next)
    );

  return router;
};

export { initPost };
