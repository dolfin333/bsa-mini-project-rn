const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  LIKE: '/like',
  DISLIKE: '/dislike'
};

export { PostsApiPath };
