import passport from 'passport';

const changePassword = passport.authenticate('change-password', { session: false });

export { changePassword };
